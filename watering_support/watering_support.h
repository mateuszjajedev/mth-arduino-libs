#ifndef _WATERING_SUPPORT_H_
#define _WATERING_SUPPORT_H_

unsigned long pipeFillingTime(int lengthInCentimeters);

#endif //_WATERING_SUPPORT_H_