#ifndef _MHT_PRINTLN_H_
#define _MHT_PRINTLN_H_

void initUSB();

void println(const char *label);
void println(const char *label, unsigned long value);

void println(const char *label, unsigned long value, const char *nextLabel);

void println(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue);
void println(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue, const char *nextLabel2, unsigned long nextValue2);

void print(const char *label);
void print(const char *label, unsigned long value);
void print(const char *label, unsigned long value, const char *nextLabel);
void print(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue);
void print(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue, const char *nextLabel2, unsigned long nextValue2);


#endif //_MHT_PRINTLN_BCK_H_
