#include "Arduino.h"

void initUSB() {
#ifdef SERIAL_ENABLED
    Serial.begin(9600);
#endif
}

void println(const char *label) {
#ifdef SERIAL_ENABLED
    Serial.println(label);
#endif
}

void println(const char *label, unsigned long value) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.println(value);
#endif
}

void println(const char *label, unsigned long value, const char *nextLabel) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.print(value);
    Serial.println(nextLabel);
#endif
}

void println(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.print(value);
    Serial.print(nextLabel);
    Serial.println(nextValue);
#endif
}

void
println(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue, const char *nextLabel2,
        unsigned long nextValue2) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.print(value);
    Serial.print(nextLabel);
    Serial.print(nextValue);
    Serial.print(nextLabel2);
    Serial.println(nextValue2);
#endif
}

void print(const char *label) {
#ifdef SERIAL_ENABLED
    Serial.println(label);
#endif
}

void print(const char *label, unsigned long value) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.println(value);
#endif
}

void print(const char *label, unsigned long value, const char *nextLabel) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.print(value);
    Serial.println(nextLabel);
#endif
}

void print(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.print(value);
    Serial.print(nextLabel);
    Serial.println(nextValue);
#endif
}

void
print(const char *label, unsigned long value, const char *nextLabel, unsigned long nextValue, const char *nextLabel2,
      unsigned long nextValue2) {
#ifdef SERIAL_ENABLED
    Serial.print(label);
    Serial.print(value);
    Serial.print(nextLabel);
    Serial.print(nextValue);
    Serial.print(nextLabel2);
    Serial.println(nextValue2);
#endif
}
