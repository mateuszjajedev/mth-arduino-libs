#include "Arduino.h"

void powerLEDs(int howMany, int *leds, int ledsAmount) {
    int states[ledsAmount];
    for (int i = 0; i < ledsAmount; ++i) states[i] = LOW;
    for (int i = 0; i < howMany; ++i) {
        states[i] = HIGH;
    }
    for (int i = 0; i < ledsAmount; ++i) {
        digitalWrite(leds[i], states[i]);
    }
}

void waitingLEDs(int *leds, int ledsAmount) {
    for (int a = 0; a < 3; ++a) {
        powerLEDs(0, leds, ledsAmount);
        for (int i = 0; i < ledsAmount; ++i) {
            digitalWrite(leds[i], HIGH);
            delay(200);
            digitalWrite(leds[i], LOW);
        }
    }
}

void initLEDs(int *leds, int ledsAmount) {
    for (int i = 0; i < ledsAmount; ++i) {
        pinMode(leds[i], OUTPUT);
        digitalWrite(leds[i], LOW);
    }
    for (int i = 0; i < 3; ++i) {
        powerLEDs(ledsAmount, leds, ledsAmount);
        delay(200);
        powerLEDs(0, leds, ledsAmount);
        delay(200);
    }
    waitingLEDs(leds, ledsAmount);
}
