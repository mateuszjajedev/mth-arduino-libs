#ifndef _MHT_LEDS_H_
#define _MHT_LEDS_H_

#include "Arduino.h"

/**

 USAGE:

 having:
    int leds[] = {2, 3, 4, 5, 6, 7, 8};
    int ledsN = sizeof(leds) / sizeof(leds[0]);

 you should init them:
    initLEDs(leds, ledsN);

 options:
    waitingLEDs(leds, ledsN);

*/

void powerLEDs(int howMany, int *leds, int ledsAmount);

void waitingLEDs(int *leds, int ledsAmount);

void initLEDs(int *leds, int ledsAmount);

#endif //_MHT_LEDS_H_